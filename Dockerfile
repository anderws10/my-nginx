FROM nginx:1.13.0-alpine

COPY ./templates/index.html /usr/share/nginx/html/index.html

EXPOSE 80